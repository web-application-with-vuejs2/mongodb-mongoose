const mongoose = require('mongoose')
const Room = require('./models/Room')
// const Building = require('./models/Building')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  // update
  // findById
  const uproom = await Room.findById('624977f57141f51df2cb9263')
  uproom.capacity = 20
  uproom.save()
  console.log(uproom)

  // findByOne
  const room = await Room.findOne({ name: '3c01' })
  console.log(room)

  // find gt=มากกว่า
  const rooms = await Room.find({ capacity: { $gt: 100 } })
  console.log(rooms)

  // delete
  const delrooms = await Room.findByIdAndDelete({ capacity: { $gt: 100 } })
  console.log(delrooms)
}

main().then(() => {
  console.log('Finish')
})
